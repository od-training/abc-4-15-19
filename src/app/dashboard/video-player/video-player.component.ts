import { Component, Input, OnChanges } from '@angular/core';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

import { Video } from '../dashboard-types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnChanges {

  @Input() selectedVideo: Video;
  safeUrl: SafeUrl;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnChanges() {
    if (this.selectedVideo) {
      this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
        'https://www.youtube.com/embed/' + this.selectedVideo.id
      );
    }
  }

}
