import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from '../dashboard-types';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {

  videosList: Observable<Video[]>;
  videoSelected: Video;

  constructor(svc: VideoService) {
    this.videosList = svc.loadVideos();
  }

  ngOnInit() {
  }

  selectVideo(video: Video) {
    this.videoSelected = video;
  }

}
