import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {

  group: FormGroup;

  constructor(fb: FormBuilder) {
    this.group = fb.group({
      region: ['all'],
      fromDate: ['1995-01-01'],
      toDate: ['2019-01-01'],
      under18: [true],
      from18to40: [true],
      from40to60: [true],
      over60: [true],
    });
  }

  ngOnInit() {
  }

}
